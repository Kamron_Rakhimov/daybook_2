from django.urls import path
from . import views


urlpatterns = [
	path('', views.index, name='index'),
	path('register/', views.register, name="register"),
	path('profile/', views.profile_detail, name='profile_detail' ),
	path('news/<slug:slug>/', views.post_detail, name="post_detail"),
	path('categoies/<slug:slug>/', views.category_detail, name="category_detail"),
	path('news/<slug:slug>/comment', views.comment, name="comment"),
	]