from django.shortcuts import render
from django.shortcuts import redirect, render, redirect 
from .models import  Post
from django.contrib.auth.models import User
from .forms import RegisterForm 
from django.db.models import Q
# Create your views here.

def index(request):
	posts = Post.objects.filter(publish=True).order_by('-date')
	return render(request, 'blog/index.html', {'posts':posts})


def post_detail(request, slug):
	post = Post.objects.filter(publish=True).get(slug=slug)
	return render(request, 'blog/post_detail.html', {'post':post})

def profile_detail(request, slug):
	post = User.objects.filter(publish=True).get(slug=slug)
	return render(request, 'blog/profile_detail.html', {'post':post})


def register(request):
	if request.user.is_authenticed:
		return redirect('index')
	if request.method == "POST":
		form =RegisterForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('index')
			
	form = RegisterForm()
	return render(request, 'blog/register.html', {'form': form})