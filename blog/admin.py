from django.contrib import admin
from .models import Post, Category, Comment

class SLugAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug':('title',)}

admin.site.register(Post, SLugAdmin)
admin.site.register(Category, SLugAdmin)
admin.site.register(Comment)

# Register your models here.
