from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class RegisterForm(UserCreationForm):
	username = forms.CharField(label='nikname', required=True, widget=forms.TextInput(attrs={'class':'from-control'})),
	email = forms.CharField(label='email', required=True, widget=forms.TextInput(attrs={'class':'from-control'})),
	first_name = forms.CharField(label='name', required=True, widget=forms.TextInput(attrs={'class':'from-control'})),
	last_name = forms.CharField(label='surname', required=True, widget=forms.TextInput(attrs={'class':'from-control'})),
	password1 = forms.CharField(label='password', required=True, widget=forms.PasswordInput(attrs={'class':'from-control'})),
	password2 = forms.CharField(label='password', required=True, widget=forms.PasswordInput(attrs={'class':'from-control'})),
	
	
	class Meta:
		model = User
		fields = ('username', 'email', 'first_name', 'last_name', 'password', 'password2',)

		def save(self, commit=True):
			user = super().save(commit=False)
			user.email = self.cleaned_data['email']
			user.frist_name = self.cleaned_data['first_name']
			user.last_name = self.cleaned_data['last_name']
			user.save()
			return user