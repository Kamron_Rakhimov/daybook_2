from django.db import models
from django.utils import timezone
from django.shortcuts import reverse
from django.contrib.auth.models import User
 


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_pic = models.ImageField('Аватарка', null=True, blank=True)
    summary = models.TextField('Описание')

    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profile"

    def link(self):
        return reverse('Profile_detail', kwargs={"slug":self.slug})

    def __str__(self):
        return self.title
    

class Category(models.Model):
    title = models.CharField('Загаловок', max_length=225)
    slug= models.SlugField("Ссылка", unique=True)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def link(self):
        return reverse('category_detail', kwargs={"slug":self.slug})

    def __str__(self):
        return self.title

class Post(models.Model):
    user = models.ForeignKey('Автор', on_delete=models.CASCADE )
    title = models.CharField('Загаловок', max_length=225)
    slug= models.SlugField("Ссылка", unique=True)
    content = models.TextField("Контент")
    publish =models.BooleanField("Опубликовано", default=False)
    date = models.DateTimeField("Дата", default=timezone.now)
    image = models.ImageField("Картинка", null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    views = models.IntegerField("Просмотры", default=0)
    

    class Meta:
        verbose_name = "Пост"
        verbose_name_plural = "Посты"

    def link(self):
        return reverse('post_detail', kwargs={"slug":self.slug})

    def __str__(self):
        return self.title    

class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Автор')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name='Пост')
    text = models.TextField('Текст')
    date = models.DateTimeField('Дата', default=timezone.now)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    def __str__(self):
         return self.user.username + ' | ' + self.post.title



class Favourite(models.Model):
    user = models.ForeignKey('Пользователь', related_name='Избранные')
    view = models.ForeignKey(User, )

    class Meta:
        verbose_name = 'Избранний'
        verbose_name_plural = 'Избранные'

    def __str__(self):
         return self.user.username + ' | ' + self.post.title



class Rate(models.Model):
    user = models.ForeignKey('Пользователь', related_name='Избранные') 

    class Meta:
        verbose_name = 'rate'
        verbose_name_plural = 'rate'

    def __str__(self):
         return self.user.username + ' | ' + self.post.title





# Create your models here.
